<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\MerchantPromotion;
use App\Models\ProductCategory;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(){
        return view('auth.login');
    }

    /**
     * Login
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
//        dd($request);
        // $this->validate($request, [
        //     'username' => 'required|max:50',
        //     'password' => 'required|alpha_dash|max:20',
        // ]);

        $remember = false;

        if ($request->get('remember', false)) {
            $remember = true;
        }

        $attempt = [
            'password' => $request->password,
            'email' => $request->email
        ];

        $email_login = $request->get('email');
        $user = User::query()->where('email', $email_login)->first();
// dd($user, Hash::check($request->password, $user->password), Auth::attempt($attempt, $remember));
        if (!$user) {
            $error = 'Tài khoản không tồn tại trên hệ thống';

            return redirect()->route('login')->with('error', $error);
        }

        if(Hash::check($request->password, $user->password)){
            if (Auth::attempt($attempt, $remember)) {
                return redirect()->route('product.index');
            } else {
                $error = 'Thông tin tài khoản không chính xác';

                return redirect()->route('login')->with('error', $error);
            }
        } else {
            $error = 'Mật khẩu không chính xác';

            return redirect()->route('login')->with('error', $error);
        }
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect('/login');
    }

    public function register(Request $request){
        $request->validate([
            'affiliate_id' => 'required',
            'shop_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ]);
        $aff_id     = $request->affiliate_id;
        $shop_name  = $request->shop_name;
        $avatar     = $request->avatar;
        $email      = $request->email;
        $phone      = $request->phone;
        $shop_slug  = Str::slug($shop_name);
        $merchant_list = $request->merchant_list;
        $categories = $request->categories;
        
        $check = User::where('affiliate_id', $aff_id)->orWhere('shop_name_slug', $shop_slug)->exists();
        if($check) return response()->json(['message' => 'Affiliate đã được đăng ký!'],400);

        try {
            DB::beginTransaction();
            $user = User::query()->create([
                "affiliate_id"      => $aff_id,
                "shop_name"         => $shop_name,
                "shop_name_slug"    => $shop_slug,
                "email"             => $email,
                "avatar"            => $avatar,
                "password"          => Hash::make("123456"),
                "phone"             => $phone,
                "avatar"            => $request->avatar           
            ]);
    
            $insertCat = [];
            foreach ($categories as $value) {
                $words = explode("-", Str::slug($value));
                $category_code = "";
                foreach ($words as $word) {
                    $category_code .= strtoupper(substr($word, 0, 1));
                }
                $insertCat[] = [
                    "category_name" => $value,
                    "affiliate_id"  => $aff_id,
                    "category_code" => $category_code
                ];
            }
            if(trim($merchant_list)){
                MerchantPromotion::create([
                    "affiliate_id" => $aff_id,
                    "merchant_list" => trim($merchant_list)
                ]);
            }
            if(count($insertCat) > 0){
                ProductCategory::insert($insertCat);
            }
            DB::commit();
            return $user;
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json(['message' => 'Có lỗi xảy ra'],500);
        }
        
    }
}
