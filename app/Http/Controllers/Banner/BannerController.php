<?php


namespace App\Http\Controllers\Banner;


use App\Http\Controllers\Controller;

use App\Models\Banner;
use Aws\S3\S3Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BannerController extends Controller
{
    private $_banner;

    public function __construct(Banner $bannerModel)
    {
        $this->_banner = $bannerModel;
    }

    public function index() {
        $listBanner = $this->_banner->getListBanner();
        return view('banner.index', compact('listBanner'));
    }

    public function editBanner(Request $request){
//        dd($request);
        $message = '';
        $data = [];
        if ($request->hasFile('url_image')) {
            //upload image to s3
            $message = $this->uploadImageToS3($request);
        } else {
            $image = $this->_banner->getBanner($request->product_id);
            if ($request->edit_image_bk != $image) {
                $message = $request->edit_image_bk;
                $data['url_image'] = $message;
            } else {
                $message = $image;
            }
        }
        $data['id'] = $request->edit_banner_id;
        $data['title'] = $request->edit_title;
        $data['description'] = $request->edit_desc;
        $data['url_image'] = $message;
        $data['link'] = $request->edit_link;

        $result = $this->_banner->updateBanner($data);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Success"], 200);
        }
        return response()->json(['status' => 400, 'data' => 'Error'], 400);
    }

    public function deleteBanner(Request $request) {
//        dd($request);
        $result = $this->_banner->deleteBanner($request->id);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Success"], 200);
        }
        return response()->json(['status' => 400, 'data' => 'Error'], 400);
    }

    public function addBanner(Request $request) {
        $message = '';
        if ($request->hasFile('url_image')) {
            //upload image to s3
            $message = $this->uploadImageToS3($request);
        }

        //setup data insert product
        $data = [];
        $data['title'] = $request->add_title;
        $data['description'] = $request->add_desc;
        $data['id_shop'] = Auth::user()->id;
        $data['url_image'] = $message;
        $data['link'] = $request->add_link;
        $result = $this->_banner->addBanner($data);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Success"], 200);
        }
        return response()->json(['status' => 400, 'data' => 'Error'], 400);
    }

    public function uploadImageToS3($request) {
        $file = $request->file('url_image');
        $fileName = 'avt' . time() . $file->getClientOriginalName();
        $filePath = $file->getPathname();
        $bucket = env('AWS_BUCKET');

        $s3 = new S3Client([
            'credentials' => [
                'key'    => env('AWS_ACCESS_KEY_ID'),
                'secret' => env('AWS_SECRET_ACCESS_KEY')
            ],
            'region' => env('AWS_REGION'),
            'version' => '2006-03-01',//latest|version
        ]);

        $key = $fileName;
        $upload = $s3->putObject(array(
            'Bucket' => $bucket,
            'Key'    => $key,
            'ACL' => 'public-read',
            'StorageClass' => 'REDUCED_REDUNDANCY',
            'SourceFile' => $filePath,
        ));
        $message = htmlspecialchars($upload->get('ObjectURL'));
        return $message;
    }
}
