<?php


namespace App\Http\Controllers\Customer;

//use App\Providers\RouteServiceProvider;
use App\Models\Customer;
use Aws\S3\S3Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    private $_customer;

    public function __construct(Customer $customer){
        $this->_customer = $customer;
    }

    public function customerProfile(){
        $infoCustomer = $this->_customer->getInfoCustomer();
//        dd();
        return view('customer.profile', compact('infoCustomer'));
    }

    public function editCustomer(Request $request) {
        $message = '';
        $data = [];
        $data['shop_name'] = $request->shop_name;
        $data['owner_name'] = $request->owner_name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['location'] = $request->location;
        $data['desc'] = $request->desc;
        $data['youtube'] = $request->youtube;
        $data['facebook'] = $request->facebook;
        $data['instagram'] = $request->instagram;
        $data['tiktok'] = $request->tiktok;
        $data['website'] = $request->website;
        $result = $this->_customer->updateProfile($data);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Success"], 200);
        }
        return response()->json(['status' => 400, 'data' => 'Error'], 400);
    }

    public function changeAvatar(Request $request) {
        $message = '';
        if ($request->hasFile('image')) {
            $message = $this->uploadImageToS3($request);
        }
        if (empty($message)) {
            return response()->json(['status' => 400, 'data' => 'Error'], 400);
        }
        $data = [];
        $data['avatar'] = $message;
        $result = $this->_customer->changeAvatar($data);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Success"], 200);
        }
        return response()->json(['status' => 400, 'data' => 'Error'], 400);
    }

    public function uploadImageToS3($request) {
        $file = $request->file('image');
        $fileName = 'avt' . time() . $file->getClientOriginalName();
        $filePath = $file->getPathname();
        $bucket = env('AWS_BUCKET');

        $s3 = new S3Client([
            'credentials' => [
                'key'    => env('AWS_ACCESS_KEY_ID'),
                'secret' => env('AWS_SECRET_ACCESS_KEY')
            ],
            'region' => env('AWS_REGION'),
            'version' => '2006-03-01',//latest|version
        ]);

        $key = $fileName;
        $upload = $s3->putObject(array(
            'Bucket' => $bucket,
            'Key'    => $key,
            'ACL' => 'public-read',
            'StorageClass' => 'REDUCED_REDUNDANCY',
            'SourceFile' => $filePath,
        ));
        $message = htmlspecialchars($upload->get('ObjectURL'));
        return $message;
    }
}
