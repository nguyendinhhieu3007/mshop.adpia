<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class MiniShopController extends Controller
{
    public function minishop($slug)
    {
        $user = User::where('shop_name_slug', $slug)->first();
        if(!$user) return redirect(route('dashboard'));
        return view('minishop.index', compact('user'));
    }
}
