<?php


namespace App\Http\Controllers\Product;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Aws\S3\S3Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    private $_product;
    private $_category;

    public function __construct(Product $productModel, Category $categoryModel)
    {
        $this->_product = $productModel;
        $this->_category = $categoryModel;
    }

    public function index() {
        $listProduct = $this->_product->getListProduct();
        $listCategory = $this->_category->getListCategory();
        return view('product.index', compact('listProduct', 'listCategory'));
    }

    public function editProduct(Request $request){
        $message = '';
        $data = [];
        $host = parse_url($request->edit_url);
        if (strpos($host['host'], $request->edit_merchant) === false){
            return response()->json(['status' => 400, 'data' => 'Error'], 400);
        }

        if ($request->hasFile('image')) {
            //upload image to s3
           $message = $this->uploadImageToS3($request);
        } else {
            $image = $this->_product->getProduct($request->product_id);
            if ($request->edit_image_bk != $image) {
                $message = $request->edit_image_bk;
                $data['image'] = $message;
            } else {
                $message = $image;
            }
        }

        $url_tracking = "https://click.adpia.vn/tracking.php?m=".$request->edit_merchant."&a=".Auth::user()->affiliate_id."&l=9999&l_cd1=3&l_cd2=0&u_id=&tu=".urlencode($request->edit_url);
        $data['product_name'] = $request->edit_product_name;
        $data['product_id'] = $request->edit_product_id;
        $data['merchant_id'] = $request->edit_merchant;
        $data['price'] = $request->edit_price;
        $data['url'] = $request->edit_url;
        $data['category_id'] = $request->edit_category;
        $data['url_tracking'] = $url_tracking;

        $result = $this->_product->updateProduct($data);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Success"], 200);
        }
        return response()->json(['status' => 400, 'data' => 'Error'], 400);
    }

    public function deleteProduct(Request $request) {
        $result = $this->_product->deleteProduct($request->id);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Success"], 200);
        }
        return response()->json(['status' => 400, 'data' => 'Error'], 400);
    }

    public function addProduct(Request $request) {
        $message = '';
        $host = parse_url($request->add_url);

        if (strpos($host['host'], $request->add_merchant) === false){
            return response()->json(['status' => 400, 'data' => 'Error'], 400);
        }

        if ($request->hasFile('image')) {
            //upload image to s3
            $message = $this->uploadImageToS3($request);
        }

        //tracking
        $url_tracking = "https://click.adpia.vn/tracking.php?m=".$request->add_merchant."&a=".Auth::user()->affiliate_id."&l=9999&l_cd1=3&l_cd2=0&u_id=&tu=".urlencode($request->add_url);

        //setup data insert product
        $data = [];
        $data['product_name'] = $request->add_product_name;
        $data['merchant_id'] = $request->add_merchant;
        $data['price'] = $request->add_price;
        $data['image'] = $message;
        $data['url'] = $request->add_url;
        $data['category_id'] = $request->add_category;
        $data['affiliate_id'] = Auth::user()->affiliate_id;
        $data['url_tracking'] = $url_tracking;
        $result = $this->_product->addProduct($data);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Success"], 200);
        }
        return response()->json(['status' => 400, 'data' => 'Error'], 400);
    }

    public function uploadImageToS3($request) {
        $file = $request->file('image');
        $fileName = 'avt' . time() . $file->getClientOriginalName();
        $filePath = $file->getPathname();
        $bucket = env('AWS_BUCKET');

        $s3 = new S3Client([
            'credentials' => [
                'key'    => env('AWS_ACCESS_KEY_ID'),
                'secret' => env('AWS_SECRET_ACCESS_KEY')
            ],
            'region' => env('AWS_REGION'),
            'version' => '2006-03-01',//latest|version
        ]);

        $key = $fileName;
        $upload = $s3->putObject(array(
            'Bucket' => $bucket,
            'Key'    => $key,
            'ACL' => 'public-read',
            'StorageClass' => 'REDUCED_REDUNDANCY',
            'SourceFile' => $filePath,
        ));
        $message = htmlspecialchars($upload->get('ObjectURL'));
        return $message;
    }

    public function util_do_upload(Request $request)
    {
        $config['upload_path'] = 'util';
        $config['allowed_types'] = null;

        if($request->f_format == 'doc')
        {
            $config['allowed_types'] = 'xlsx|xls|pdf|doc|docx|txt|ppt|pptx';
        }

        if($request->f_format == 'img')
        {
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
        }

        if($request->f_format == 'flash')
        {
            $config['allowed_types'] = 'swf';
        }

        if($request->f_format == 'cmp')
        {
            $config['allowed_types'] = 'zip|rar|tgz';
        }

        $view = 'util.do_upload';

        if(isset($_POST) && !empty($_FILES['file']))
        {


            $bucket = env('AWS_BUCKET');
            $s3 = new S3Client([
                'credentials' => [
                    'key'    => env('AWS_ACCESS_KEY_ID'),
                    'secret' => env('AWS_SECRET_ACCESS_KEY')
                ],
                'region' => env('AWS_DEFAULT_REGION'),
                'version' => '2006-03-01',//latest|version
            ]);

            $date = date('Ymd', time());

            $key = $config['upload_path'] . '/' . 'upload_document' . '/'. $date . '/' . str_replace(' ', '-', $path);
            $upload = $s3->putObject(array(
                'Bucket' => $bucket,
                'Key'    => $key,
                'Body' => fopen($_FILES['file']['tmp_name'], 'r+'),
                'ACL' => 'public-read',
                'ContentType' => $_FILES['file']['type'],
                'StorageClass' => 'REDUCED_REDUNDANCY',
            ));

            $message = htmlspecialchars($upload->get('ObjectURL'));
            return view($view, compact('view', 'message'));
        }
        else
        {
            $message =  'Not file';
            return view($view, compact('view', 'message'));
        }
    }
}
