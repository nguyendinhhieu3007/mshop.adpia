<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Banner extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "mshop_banner";
    protected $guarded = [];

    public function addBanner($data) {
        return Banner::create($data);
    }

    public function getListBanner() {
        return Banner::query()->join('mshop_info','mshop_banner.id_shop','=','mshop_info.id')
            ->select('mshop_banner.*', 'mshop_info.shop_name')
            ->where('mshop_banner.id_shop', Auth::user()->id)
            ->whereNull('mshop_banner.deleted_at')
            ->get()->toArray();
    }

    public function getBanner($id){
        return Banner::query()->select('url_image')->where('id', $id)->first();
    }

    public function updateBanner($data) {
        return Banner::where('id',$data['id'])->update([
            'title' => $data['title'],
            'description' => $data['description'],
            'url_image' => $data['url_image'],
            'link' => $data['link'],
        ]);
    }
    public function deleteBanner($id) {
        return Banner::query()->where('id',$id)->delete();
    }

}
