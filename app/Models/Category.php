<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Category extends Model
{

    protected $table = 'mshop_product_category';

    public function getListCategory() {
        return Category::query()->where('affiliate_id', Auth::user()->affiliate_id)->get()->toArray();
    }
}
