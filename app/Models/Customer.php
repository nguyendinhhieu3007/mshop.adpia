<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Customer extends Model
{
    use SoftDeletes;
    protected $table = 'mshop_info';
    protected $guarded = [];

    public function getInfoCustomer() {
        return Customer::query()->where('affiliate_id', Auth::user()->affiliate_id)->first();
    }

    public function changeAvatar($data) {
        return Customer::query()->where('affiliate_id', Auth::user()->affiliate_id)
            ->update([
                'avatar' => $data['avatar']
            ]);
    }

    public function updateProfile($data) {
        return Customer::query()->where('affiliate_id', Auth::user()->affiliate_id)
            ->update([
                'shop_name' => $data['shop_name'],
                'owner_name' => $data['owner_name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'location' => $data['location'],
                'desc' => $data['desc'],
                'youtube' => $data['youtube'],
                'facebook' => $data['facebook'],
                'instagram' => $data['instagram'],
                'tiktok' => $data['tiktok'],
                'website' => $data['website']
            ]);
    }
}
