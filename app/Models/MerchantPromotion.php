<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerchantPromotion extends Model
{
    use HasFactory;

    protected $table = 'mshop_merchant_promo_subs';
    protected $guarded = [];
}
