<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'mshop_product';
    protected $guarded = [];
//    public $timestamps = false;

    public function getListProduct() {
        return Product::query()->join('mshop_product_category','mshop_product.category_id','=','mshop_product_category.id')
            ->select('mshop_product.*', 'mshop_product_category.category_name')
            ->where('mshop_product.affiliate_id', Auth::user()->affiliate_id)
            ->whereNull('mshop_product.deleted_at')
            ->get()->toArray();
    }

    public function getProduct($id){
        return Product::query()->select('image')->where('id', $id)->first();
    }

    public function updateProduct($data) {
        return Product::where('id',$data['product_id'])->update([
            'product_name' => $data['product_name'],
            'merchant_id' => $data['merchant_id'],
            'price' => $data['price'],
            'image' => $data['image'],
            'url' => $data['url'],
            'category_id' => $data['category_id'],
        ]);
    }

    public function addProduct($data) {
        return Product::create($data);
    }

    public function deleteProduct($id) {
        return Product::query()->where('id',$id)->delete();
    }
}
