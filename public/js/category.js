let __cateArray = [];
let __categoryActive = 0;

$(document).ready(() => {

  let affId = $("input[name=aff_id]").val();
  getCategory(affId);
});

function getCategory(affId) {
  fetch(`https://mapi.adpia.vn/v2/mshop/getMshopCategory?aff_id=${affId}`)
    .then((response) => {
      return response.json();
    })
    .then(function (responseAsJson) {
      if (
        responseAsJson &&
        responseAsJson.items &&
        responseAsJson.items.length > 0
      ) {
        __cateArray = responseAsJson.items;
      }
    });
}

function handleProductCategory(id, obj) {
  $("input[name=product_category]").val(id);
  shimmerProduct();
  getProduct();
  homeNavClick();
  __categoryActive = id;
}
