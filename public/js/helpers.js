toastr.options = {
  closeButton: false,
  debug: false,
  newestOnTop: true,
  progressBar: true,
  positionClass: "toast-top-center",
  preventDuplicates: false,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut"
};
function dateFormat(date) {
  let d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [day, month, year].join("/");
}

function convertLinkTracking(mid, link) {
  let trackingDomain = "https://click.adpia.vn/tracking.php";
  let affId = $("input[name=aff_id]").val() ?? 'A100081167';
  let lParam = "9999";
  let lCd1Param = "3";
  let lCd2Param = "0";
  let uIdParam = "";
  let tu = encodeURIComponent(link);
  return `${trackingDomain}?m=${mid}&a=${affId}&l=${lParam}&l_cd1=${lCd1Param}&l_cd2=${lCd2Param}&u_id=${uIdParam}&tu=${tu}`;
}

function copyToClipboard(content) {
  // Create a "hidden" input
  var aux = document.createElement("input");

  // Assign it the value of the specified element
  aux.setAttribute("value", content);

  // Append it to the body
  document.body.appendChild(aux);

  // Highlight its content
  aux.select();

  // Copy the highlighted text
  document.execCommand("copy");

  // Remove it from the body
  document.body.removeChild(aux);
}

function redirectLinkTracking(e) {
  e.preventDefault();

  let code = $(e.target).html().trim();

  let isCopy = true;
  if (
    code === "LẤY ƯU ĐÃI" ||
    code === "Lấy ở trang mở ra" ||
    code === "Thu thập trên app"
  ) {
    isCopy = false;
  }
  
  if (isCopy) {
    $(e.target).html('<i class="fa fa-spinner"></i> Copying...');
  } else {
    $(e.target).html('<i class="fa fa-spinner"></i> Chờ chuyển hướng...');
  }

  $(e.target).prop("disabled", true);

  let url = $(e.target).attr("data-url");
  let mid = $(e.target).attr("data-mid");

  url = url.replace(/-/g, "%2D");

  let linkTracking = convertLinkTracking(mid, url);

  //window.open(linkTracking);

  let postData = {
    dynamicLinkInfo: {
      domainUriPrefix: "https://adpvn.co",
      link: linkTracking
    },
    suffix: {
      option: "SHORT"
    }
  };

  fetch(
    "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyBbazb9MrveabPlmtoYO-0M1GFTJcePWzA",
    {
      method: "POST",
      body: JSON.stringify(postData),
      headers: {
        "Content-Type": "application/json"
      }
    }
  )
    .then((response) => {
      return response.json();
    })
    .then(function (responseAsJson) {
      if (responseAsJson && responseAsJson.shortLink) {
        $(e.target).html(code);
        $(e.target).prop("disabled", false);

        if (isCopy) {
          copyToClipboard(code);

          toastr.success(
            "Sau 2s sẽ tự động chuyển trang...",
            "Sao chép thành công!"
          );
        } else {
          toastr.success(
            "Sau 2s sẽ tự động chuyển trang...",
            "Chờ chuyển hướng!"
          );
        }

        setTimeout(() => {
          window.open(responseAsJson.shortLink, "_blank");
        }, 2000);
      }
    });
}

function addCommas(nStr) {
  nStr += "";
  let x = nStr.split(".");
  let x1 = x[0];
  let x2 = x.length > 1 ? "." + x[1] : "";
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, "$1" + "," + "$2");
  }
  return x1 + x2;
}
