let __tabActive = 2;
let __lastPageActivePromoCode = 1;
let __lastPageActiveProduct = 1;
let __tabManagerBottomActive = 0;

// switch to product tab
$("input[id=radio-1]").change((e) => {
  __tabActive = 2;
  $(".cdp_i.active").html(__lastPageActiveProduct);
  $(".promo-code-tab").animate(
    {
      opacity: 0
    },
    200,
    () => {
      $(".promo-code-tab").css("display", "none");
      $(".product-tab").css("display", "block");

      $(".product-tab").animate(
        {
          opacity: 1
        },
        200
      );
    }
  );
});

// switch to promo tab
$("input[id=radio-2]").change((e) => {
  __tabActive = 1;
  $(".cdp_i.active").html(__lastPageActivePromoCode);
  $(".product-tab").animate(
    {
      opacity: 0
    },
    200,
    () => {
      $(".product-tab").css("display", "none");
      $(".promo-code-tab").css("display", "block");

      $(".promo-code-tab").animate(
        {
          opacity: 1
        },
        200
      );
    }
  );
});

function homeNavClick() {
  $(".nav-bottom .tabbar label:nth-child(2)").click();
}
