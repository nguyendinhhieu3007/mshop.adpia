let __merchantPromoSubsArray = [];
let __merchantPromoSubsActive = 0;

$(document).ready(() => {
  let affId = $("input[name=aff_id]").val();
  getMerchantPromoSubs(affId);
});

function getMerchantPromoSubs(affId) {
  fetch(
    `https://mapi.adpia.vn/v2/mshop/getMshopMerchantPromoSubs?aff_id=${affId}`
  )
    .then((response) => {
      return response.json();
    })
    .then(function (responseAsJson) {
      if (
        responseAsJson &&
        responseAsJson.items &&
        responseAsJson.items.merchant_list
      ) {
        __merchantPromoSubsArray = responseAsJson.items.merchant_list.split(
          ","
        );
      }
    });
}

function handleMerchantPromoSubs(id, obj) {
  $("input[name=merchant_promo_subs]").val(id);
  shimmerPromo();
  getPromoCodes();
  homeNavClick();
  __merchantPromoSubsActive = id;
}
