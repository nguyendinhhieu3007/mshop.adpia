function goNextPage() {
  let currentPage = parseInt($(".cdp_i.active").html(), 10);

  if (
    (currentPage === __lastPagePromoCode && __tabActive === 1) ||
    (currentPage === __lastPageProduct && __tabActive === 2)
  ) {
    return false;
  }

  $(".cdp_i.active").html(currentPage + 1);

  switch (__tabActive) {
    case 1:
      shimmerPromo();
      getPromoCodes(currentPage + 1);
      __lastPageActivePromoCode = currentPage + 1;
      break;
    case 2:
      shimmerProduct();
      getProduct(currentPage + 1);
      __lastPageActiveProduct = currentPage + 1;
      break;
    default:
      return false;
  }
}

function goPrevPage() {
  let currentPage = parseInt($(".cdp_i.active").html(), 10);

  if (currentPage === 1) {
    return false;
  }

  $(".cdp_i.active").html(currentPage - 1);

  switch (__tabActive) {
    case 1:
      shimmerPromo();
      getPromoCodes(currentPage - 1);
      __lastPageActivePromoCode = currentPage - 1;
      break;
    case 2:
      shimmerProduct();
      getProduct(currentPage - 1);
      __lastPageActiveProduct = currentPage - 1;
      break;
    default:
      return false;
  }
}

function goCurrentPage() {
  let page = 0;

  if (__tabActive === 1) {
    page = parseInt($(".pagination-go-page input").eq(0).val(), 10);

    if (page > __lastPagePromoCode) {
      page = __lastPagePromoCode;
    }
  } else if (__tabActive === 2) {
    page = parseInt($(".pagination-go-page input").eq(1).val(), 10);

    if (page > __lastPageProduct) {
      page = __lastPageProduct;
    }
  }

  $(".cdp_i.active").html(page);

  switch (__tabActive) {
    case 1:
      shimmerPromo();
      getPromoCodes(page);
      __lastPageActivePromoCode = page;
      break;
    case 2:
      shimmerProduct();
      getProduct(page);
      __lastPageActiveProduct = page;
      break;
    default:
      return false;
  }
}

function shimmerPromo() {
  let currentItemsLength = $(".ticket").length;

  let shimmerHtml = "";

  for (let i = 0; i < currentItemsLength; i++) {
    shimmerHtml += `
    <div class="shimmer-item">
      <div class="promo-code-item">
        <div class="promo-code-item-left">
          <div class="shimmerBG _c-h-20px"></div>
          <div class="shimmerBG _c-h-20px _c-mt-10px"></div>
          <div class="shimmerBG _c-h-20px _c-mt-10px"></div>
        </div>
        <div class="promo-code-item-right _c-bl-none">
          <div class="shimmerBG _c-h-100pc _c-w-100pc"></div>
        </div>
      </div>
    </div>
    `;
  }

  $(".promo-code-tab-inner").html(shimmerHtml);
}

function shimmerProduct() {
  let currentItemsLength = $(".product-item").length;

  let shimmerHtml = "";

  for (let i = 0; i < currentItemsLength; i++) {
    shimmerHtml += `
    <div class="product-item">
      <div class="shimmerBG _c-h-100px _c-w-100px"></div>
      <div class="product-item-detail" style="width: calc(100% - 120px);">
        <div class="product-item-title">
          <div class="shimmerBG _c-h-30px"></div>
        </div>
        <div class="product-item-merchant">
          <div class="shimmerBG _c-h-10px"></div>
        </div>
        <div class="product-item-buy">
          <div class="product-item-price">
            <div class="shimmerBG _c-h-20px"></div>
          </div>
          <div class="product-item-go">
            <a href="" target="_blank">
              <div class="shimmerBG _c-h-20px _c-w-90px _c-ml-10px"></div>
            </a>
          </div>
        </div>
      </div>
    </div>
    `;
  }

  $(".product-tab-inner").html(shimmerHtml);
}
