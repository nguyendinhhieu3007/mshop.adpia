let __lastPageProduct = 0;
let __searchStr = "";
let __highestPrice = 0;
let __arrayMerchant = [];
let __merchantFilter = "";
let __productSort = "date";
let __productSortType = "desc";

$(document).ready(() => {

  getProduct(1, true);
});

function getProduct(page = 1, isFirstLoad = false) {
  let affId = $("input[name=aff_id]").val();
  let product_category = $("input[name=product_category]").val();
  let minPrice = $(".price-range-input input").eq(0).val() || 0;
  let maxPrice = $(".price-range-input input").eq(1).val() || 10000000;
  let mid = $("select[name=mid]").val() || "";
  let sort = $("input[name=sort]:checked").val() || "";
  let sortType = $("input[name=sort_type]:checked").val() || "";
  __merchantFilter = mid;
  fetch(
    `https://mapi.adpia.vn/v2/mshop/getMshopProduct?limit=10&aff_id=${affId}&page=${page}&cateId=${product_category}&pName=${__searchStr}&minPrice=${minPrice}&maxPrice=${maxPrice}&mid=${mid}&sort=${sort}&sortType=${sortType}`
  )
    .then((response) => {
      return response.json();
    })
    .then(function (responseAsJson) {
      let itemHtmlRow = ``;
      if (
        responseAsJson &&
        responseAsJson.items &&
        responseAsJson.items.length > 0
      ) {
        responseAsJson.items.forEach((item, index) => {
          itemHtmlRow += `
          <div class="product-item">
            <div class="product-item-image" style="background-image: url('${
              item.image
            }')"></div>
            <div class="product-item-detail">
              <div class="product-item-title">
                ${item.product_name}
              </div>
              <div class="product-item-merchant">
                ${item.merchant_id}
              </div>
              <div class="product-item-buy">
                <div class="product-item-price">
                  ${new Intl.NumberFormat().format(item.price)}&#8363;
                </div>
                <div class="product-item-go">
                  <a href="${convertLinkTracking(item.merchant_id, item.url)}" target="_blank">
                    <div class="product-item-button">
                      <i class="fas fa-shopping-cart"></i> Mua hàng
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          `;

          if (__highestPrice < parseInt(item.price)) {
            __highestPrice = parseInt(item.price);
          }

          if (!__arrayMerchant.includes(item.merchant_id)) {
            __arrayMerchant.push(item.merchant_id);
          }
        });
        if (isFirstLoad) {
          $(".product-tab-inner").html(itemHtmlRow);
        } else {
          setTimeout(() => {
            $(".product-tab-inner").html(itemHtmlRow);
          }, 500);
        }

        __lastPageProduct = parseInt(responseAsJson.lastPage, 10);

        $(".pagination-total-page")
          .eq(1)
          .html(" / " + responseAsJson.lastPage);
      } else {
        let htmlRow = `
        <div style="max-width: 100%;width: 100px;margin: auto;">
          <img src="https://cdn-icons-png.flaticon.com/512/2945/2945609.png" style="width: 100%;">
        </div>
        <div style="text-align: center;">
          Không có sản phẩm nào!
        </div>
        `;

        $(".product-tab-inner").html(htmlRow);
      }
    });
}
