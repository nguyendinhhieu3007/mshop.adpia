let __lastPagePromoCode = 0;
let __orderPromoActive = "new";

$(document).ready(() => {
  getPromoCodes(1, true);
  window.scrollTo(0, 0);
});

function getPromoCodes(page = 1, isFirstLoad = false) {
  let affId = $("input[name=aff_id]").val();
  let mid = $("input[name=merchant_promo_subs]").val() || "";
  fetch(
    `https://mapi.adpia.vn/v2/mshop/getMshopCoupon?mid=${mid}&aff_id=${affId}&page=${page}&search=${__searchStr}`
  )
    .then((response) => {
      return response.json();
    })
    .then(function (responseAsJson) {
      let itemHtmlRow = ``;

      if (
        responseAsJson &&
        responseAsJson.items &&
        responseAsJson.items.length > 0
      ) {
        responseAsJson.items.forEach((item, index) => {
          itemHtmlRow += `
          <div class="ticket">
            <div class="left"></div>
            <div class="right"></div>
            <div class="ticket-content-wrapper">
              <div class="promo-code-item">
                <div class="promo-code-item-left">
                  <div class="promo-code-item-title">
                    ${item.title}
                  </div>

                  <div class="promo-code-item-expire">
                    ${
                      item.expire !== "ĐỘC QUYỀN"
                        ? '<i class="far fa-calendar-check"></i> HSD:' +
                          dateFormat(item.expire)
                        : '<i class="far fa-star"></i> MÃ ' + item.expire
                    }
                  </div>
                  <div class="promo-code-item-button btn-handle-link" data-url="${
                    item.link
                  }" data-mid="${item.mid}">
                    ${item.code}
                  </div>
                </div>
                <div class="promo-code-item-right">
                  <div class="promo-code-item-right-inner">
                    <div class="promo-code-item-logo">
                      <img src="/images/merchant/${item.mid}.png">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          `;
        });

        $(".promo-code-tab-inner").html(itemHtmlRow);

        __lastPagePromoCode = parseInt(responseAsJson.lastPage, 10);

        if (responseAsJson.lastPage > 1) {
          $(".content_detail__pagination").css("display", "block");
          $(".pagination-total-page")
            .eq(0)
            .html(" / " + responseAsJson.lastPage);
        } else {
          $(".content_detail__pagination").css("display", "none");
        }

        if (!isFirstLoad) {
          document
            .querySelectorAll(".promo-code-tab-inner")[0]
            .scrollIntoView();
        }
      }
    });
}

$(".promo-code-tab-inner").on("click", ".btn-handle-link", (e) => {
  redirectLinkTracking(e);
});

function handleOrderPromo(order) {
  $(".tab-category-item-inner svg").attr("stroke", "#000000");
  shimmerPromo();
  getPromoCodes();
  homeNavClick();
  __orderPromoActive = order;
}
