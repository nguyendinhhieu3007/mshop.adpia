// Click tab not home
$(".nav-bottom .tabbar label:not(:nth-child(2))").click((e) => {
  let menuId = getMenuId(e);
  // If click tab has been activated
  if ($("input[id=" + menuId + "]").prop("checked")) {
    return false;
  }
  // Show tab detail
  getTabManagerBottomDetail(menuId);
  // Show dark layout
  $(".dark-layout").addClass("show");
  setDarkLayout();
  // Set current tab active
  setTabManagerBottom(e);
});

// Click tab home
$(".nav-bottom .tabbar label:nth-child(2)").click((e) => {
  // Hide dark layout
  $(".dark-layout").removeClass("show");
  setDarkLayout();
  // Set current tab active
  setTabManagerBottom(e);
});

async function setTabManagerBottom(e) {
  let menuId = getMenuId(e);

  // Show tab detail
  await getTabManagerBottomDetail(menuId);

  // If click from home
  if ($(".tab-manager-bottom").height() === 0) {
    let tabDetailHeight =
      document.querySelector(".tab-manager-bottom").scrollHeight + 100;
    if (
      ($(e.target).prop("tagName") === "use" &&
        $(e.target).attr("xlink:href") === "#homeIcon") ||
      ($(e.target).prop("tagName") === "SPAN" &&
        $(e.target).html() === "Home") ||
      ($(e.target).prop("tagName") === "LABEL" &&
        $(e.target).attr("for") === "menu-1") ||
      ($(e.target).prop("tagName") === "svg" &&
        $(e.target).attr("class").indexOf("home-icon") !== -1)
    ) {
      return false;
    }

    $(".tab-manager-bottom").stop().animate(
      {
        height: tabDetailHeight
      },
      500
    );
  } else {
    // If click from another tab not home
    // If click to home
    if (
      ($(e.target).prop("tagName") === "use" &&
        $(e.target).attr("xlink:href") === "#homeIcon") ||
      ($(e.target).prop("tagName") === "SPAN" &&
        $(e.target).html() === "Home") ||
      ($(e.target).prop("tagName") === "LABEL" &&
        $(e.target).attr("for") === "menu-1") ||
      ($(e.target).prop("tagName") === "svg" &&
        $(e.target).attr("class").indexOf("home-icon") !== -1)
    ) {
      $(".tab-manager-bottom").stop().animate(
        {
          height: "0px"
        },
        250
      );
    } else {
      // If click to another tab
      $(".tab-manager-bottom")
        .stop()
        .animate(
          {
            height: "0px"
          },
          250,
          () => {
            let tabDetailHeight =
              document.querySelector(".tab-manager-bottom").scrollHeight + 100;
            $(".tab-manager-bottom").stop().animate(
              {
                height: tabDetailHeight
              },
              250
            );
          }
        );
    }
  }
}

function getMenuId(e) {
  if (
    $(e.target).prop("tagName") === "use" &&
    $(e.target).parent().parent().prop("tagName") === "LABEL"
  ) {
    return $(e.target).parent().parent().attr("for");
  } else if (
    ($(e.target).prop("tagName") === "SPAN" &&
      $(e.target).parent().prop("tagName") === "LABEL") ||
    ($(e.target).prop("tagName") === "svg" &&
      $(e.target).attr("class").indexOf("tab-manager-bottom-icon") !== -1)
  ) {
    return $(e.target).parent().attr("for");
  } else if (
    $(e.target).prop("tagName") === "LABEL" &&
    $(e.target).attr("for").indexOf("menu") !== -1
  ) {
    return $(e.target).attr("for");
  } else {
    return "";
  }
}

// == draw each menu
function getTabManagerBottomDetail(tabId) {
  let htmlRaw = "";
  switch (tabId) {
    case "menu-2":
      htmlRaw += drawMenu2();
      break;
    case "menu-3":
      htmlRaw += drawMenu3();
      break;
    case "menu-4":
      htmlRaw += drawMenu4();
      break;

    default:
      break;
  }
  $(".tab-manager-bottom-inner").html(htmlRaw);

  // Search input animation
  if (tabId === "menu-4") {
    setTimeout(() => {
      document.querySelector("form").classList.add("active");
    }, 500);
  }

  if (tabId === "menu-3") {
    document.querySelectorAll(".price-range-input input").forEach((input) => {
      $(".tab-manager-bottom-inner").on("input", input, (e) => {
        let rangeInput = document.querySelectorAll(".price-range-input input");
        let minText = document.querySelector(".price-text-min");
        let maxText = document.querySelector(".price-text-max");
        let progress = document.querySelector(".price-slider .price-progress");
        let minVal = parseInt(rangeInput[0].value, 10);
        let maxVal = parseInt(rangeInput[1].value, 10);
        let priceGap = 50000;
        let excRate = 1;

        if (maxVal - minVal < priceGap) {
          if (e.target.className === "price-range-min") {
            rangeInput[0].value = maxVal - priceGap;
          } else {
            rangeInput[1].value = minVal + priceGap;
          }
        } else {
          rangeInput[0].value = minVal;
          rangeInput[1].value = maxVal;
          minText.innerHTML = addCommas(minVal * excRate);
          maxText.innerHTML = addCommas(maxVal * excRate);
          progress.style.left = (minVal / rangeInput[0].max) * 100 + "%";
          progress.style.right = 100 - (maxVal / rangeInput[1].max) * 100 + "%";
        }
      });
    });
  }
}

function setDarkLayout() {
  if ($(".dark-layout").attr("class").indexOf("show") !== -1) {
    $(".dark-layout").css("height", "100%");
    $(".dark-layout").stop().animate(
      {
        opacity: "1"
      },
      500
    );
  } else {
    $(".dark-layout")
      .stop()
      .animate(
        {
          opacity: "0"
        },
        500,
        () => {
          $(".dark-layout").css("height", "0px");
        }
      );
  }
}

function drawMenu2() {
  let htmlRaw = "";

  if (__tabActive === 1) {
    let htmlMerchantPromoSubs = "";

    __merchantPromoSubsArray.forEach((item) => {
      htmlMerchantPromoSubs += `
      <div class="tab-category-item _c-m-0" onclick="handleMerchantPromoSubs('${item}', this)">
          <div class="tab-category-item-inner _c-w-50px _c_border_none">
            <img class="_c-w-100pc _c_border_circle" src="/images/merchant/${item}.png">
          </div>
        </div>
      `;
    });

    htmlRaw += `
    <div class="tab-category">
    ${htmlMerchantPromoSubs}
    </div>
    `;
  } else if (__tabActive === 2) {
    let htmlCateList = "";

    __cateArray.forEach((item, index) => {
      htmlCateList += `
      <div class="tab-category-item${
        parseInt(item.id) === __categoryActive ? " active" : ""
      }" onclick="handleProductCategory(${item.id}, this)">
          <div class="tab-category-item-inner">
            ${item.category_name}
          </div>
        </div>
      `;
    });

    htmlRaw += `
      <div class="tab-category">
        <div class="tab-category-item${
          0 === __categoryActive ? " active" : ""
        }" onclick="handleProductCategory(0, this)">
          <div class="tab-category-item-inner">
            Tất cả
          </div>
        </div>
        ${htmlCateList}
      </div>
    `;
  }

  return htmlRaw;
}

function drawMenu3() {
  let htmlRaw = ``;

  if (__tabActive === 1) {
    htmlRaw += `
    <div class="tab-category">
      <div class="tab-category-item${
        __orderPromoActive === "new" ? " active" : ""
      }" onclick="handleOrderPromo('new')">
        <div class="tab-category-item-inner">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="${
            __orderPromoActive === "new" ? "#ffffff" : "#00000099"
          }" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
            <circle cx="12" cy="12" r="5"/><path d="M12 1v2M12 21v2M4.2 4.2l1.4 1.4M18.4 18.4l1.4 1.4M1 12h2M21 12h2M4.2 19.8l1.4-1.4M18.4 5.6l1.4-1.4"/>
          </svg>
          Mới nhất
        </div>
      </div>
      <div class="tab-category-item${
        __orderPromoActive === "hot" ? " active" : ""
      }" onclick="handleOrderPromo('hot')">
        <div class="tab-category-item-inner">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="${
            __orderPromoActive === "hot" ? "#ffffff" : "#00000099"
          }" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
            <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
          </svg>
          Hot nhất
        </div>
      </div>
      <div class="tab-category-item${
        __orderPromoActive === "expired" ? " active" : ""
      }" onclick="handleOrderPromo('expired')">
        <div class="tab-category-item-inner">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="${
            __orderPromoActive === "expired" ? "#ffffff" : "#00000099"
          }" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
            <path d="M21 12.79A9 9 0 1 1 11.21 3 7 7 0 0 0 21 12.79z"></path>
          </svg>
          Sắp hết hạn
        </div>
      </div>
    </div>
    `;
  } else if (__tabActive === 2) {
    let htmlMerchantOptions = `<option value="">Toàn bộ</option>`;

    __arrayMerchant.forEach((item, index) => {
      htmlMerchantOptions += `<option value="${item}"${
        item === __merchantFilter ? " selected" : ""
      }>${item}</option>`;
    });

    htmlRaw += `
  <div class="form-group _c-mt-10px _c-mb-10px">
    <div class="form-group-4">
    <label>Đơn giá(&#8363;)</label>
    </div>
    <div class="form-group-8">
      <div class="price-slider">
        <div class="price-progress"></div>
      </div>
      <div class="price-range-input">
        <input type="range" class="price-range-min" min="0" max="${__highestPrice}" value="0" step="10000">
        <input type="range" class="price-range-max" min="0" max="${__highestPrice}" value="${__highestPrice}" step="10000">
      </div>
      <div class="_c-mt-10px _c_text_center"><span class="price-text-min">${addCommas(
        0
      )}</span> - <span class="price-text-max">${addCommas(
      __highestPrice
    )}</span></div>
    </div>
  </div>
  <div class="form-group _c-mt-10px _c-mb-10px">
    <div class="form-group-4">
    <label>Nhà cung cấp</label>
    </div>
    <div class="form-group-8">
      <select class="_c-w-100pc" name="mid">${htmlMerchantOptions}</select>
    </div>
  </div>
  <div class="form-group _c-mt-10px _c-mb-10px">
    <div class="form-group-4">
    <label>Sắp xếp theo</label>
    </div>
    <div class="form-group-8">
      <div class="form-group">
        <div class="_c_text_center _c-mr-10px">
          <div>Ngày Đăng</div>
          <input type="radio" name="sort" value="date"${
            __productSort === "date" ? " checked" : ""
          }>
        </div>
        <div class="_c_text_center _c-mr-10px">
          <div>Tên sản phẩm</div>
          <input type="radio" name="sort" value="name"${
            __productSort === "name" ? " checked" : ""
          }>
        </div>
        <div class="_c_text_center">
          <div>Đơn giá</div>
          <input type="radio" name="sort" value="price"${
            __productSort === "price" ? " checked" : ""
          }>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group _c-mt-10px _c-mb-10px">
    <div class="form-group-4">
    <label>Thứ tự</label>
    </div>
    <div class="form-group-8">
      <div class="form-group">
      <div class="_c_text_center _c-mr-10px">
        <div>Giảm dần</div>
          <input type="radio" name="sort_type" value="desc"${
            __productSortType === "desc" ? " checked" : ""
          }>
        </div>
        <div class="_c_text_center">
          <div>Tăng dần</div>
          <input type="radio" name="sort_type" value="asc"${
            __productSortType === "asc" ? " checked" : ""
          }>
        </div>
      </div>
    </div>
  </div>
  <div class="filter-submit _c_text_center">
    <label onclick="handleFilter();">Lưu thay đổi</label>
  </div>
  `;
  }

  return htmlRaw;
}

function drawMenu4() {
  let htmlRaw = ``;
  let placeholder = "Nhập tên mã, link sản phẩm,...";

  if (__tabActive === 2) {
    placeholder = "Nhập tên sản phẩm...";
  }

  htmlRaw += `
  <form action="">
    <input type="search" name="search" value="${__searchStr}" placeholder="${placeholder}" required>
    <i class="fas fa-search" onclick="handleSearch()"></i>
  </form>
  `;

  return htmlRaw;
}

function handleSearch() {
  let str = $("input[name=search]").val();
  __searchStr = str;
  if (__tabActive === 1) {
    shimmerPromo();
    getPromoCodes();
  } else {
    shimmerProduct();
    getProduct();
  }
  homeNavClick();
}

function handleFilter() {
  let sort = $("input[name=sort]:checked").val() || "";
  let sortType = $("input[name=sort_type]:checked").val() || "";
  __productSort = sort;
  __productSortType = sortType;
  shimmerProduct();
  getProduct();
  homeNavClick();
}
