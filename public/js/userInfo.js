$(document).ready(() => {

  getUserInfo();
});

function getUserInfo() {
  let affId = $("input[name=aff_id]").val();
  $(".cdp").children().css("opacity", "0");

  fetch(`https://mapi.adpia.vn/v2/mshop/getMshopInfo?affiliate_id=${affId}`)
    .then((response) => {
      return response.json();
    })
    .then(function (responseAsJson) {
      if (responseAsJson && responseAsJson.data) {
        let info = responseAsJson.data;
        setTimeout(() => {
          $("html").fadeOut(500, function () {
            //wall
            $(".personal-bio-wall").removeClass("shimmerBG");
            // $(".personal-bio-wall").html(
            //   `<div class="personal-bio-name-store">${info.shop_name}</div>`
            // );
            //avatar
            $(".personal-bio-avatar-inner").removeClass("shimmerBG");
            $(".personal-bio-avatar-outline").css(
              "background",
              `linear-gradient(
                rgba(255, 255, 255, 1) 0%,
                rgba(255, 255, 255, 1) 50%,
                rgba(255, 140, 0, 1) 50%,
                rgba(255, 140, 0, 1) 100%
              )`
            );
            $(".personal-bio-avatar-inner").css(
              "background-image",
              "url('" + info.avatar + "'"
            );
            //name
            $(".personal-bio-username").removeClass("shimmerBG");
            $(".personal-bio-username span").html(info.shop_name);
            $(".personal-bio-username").css("color", "#000");
            //description
            $(".personal-bio-short-desc").html(
              info.desc && `<input type="checkbox" id="expanded">
              <span>
              ${info.desc}
              </span>
              <label onclick="handleBioDesc()" for="expanded" role="button">xem thêm</label>`
            );
            const ps = document.querySelectorAll(
              ".personal-bio-short-desc span"
            );
            const observer = new ResizeObserver((entries) => {
              for (let entry of entries) {
                entry.target.classList[
                  entry.target.scrollHeight > entry.target.offsetHeight
                    ? "add"
                    : "remove"
                ]("truncated");
              }
            });

            ps.forEach((p) => {
              observer.observe(p);
            });
            //social
            $(".personal-bio-social-item").removeClass("shimmerBG");
            // if (info.youtube && info.youtube.length > 0) {
              $(".personal-bio-social-item:nth-child(1)").html(
                `<a href="${info.youtube ?? 'https://youtube.com'}" target="_blank">
                  <img src="/images/social/youtube.png" alt="" />
                </a>`
              );
            // } else {
            //   $(".personal-bio-social-item:nth-child(1)").css(
            //     "display",
            //     "none"
            //   );
            // }

            // if (info.facebook && info.facebook.length > 0) {
              $(".personal-bio-social-item:nth-child(2)").html(
                `<a href="${info.facebook ?? 'https://facebook.com'}" target="_blank">
                  <img src="/images/social/facebook.png" alt="" />
                </a>`
              );
            // } else {
            //   $(".personal-bio-social-item:nth-child(2)").css(
            //     "display",
            //     "none"
            //   );
            // }

            // if (info.instagram && info.instagram.length > 0) {
              $(".personal-bio-social-item:nth-child(3)").html(
                `<a href="${info.instagram ?? 'https://instagram.com'}" target="_blank">
                  <img src="/images/social/instagram.png" alt="" />
                </a>`
              );
            // } else {
            //   $(".personal-bio-social-item:nth-child(3)").css(
            //     "display",
            //     "none"
            //   );
            // }

            // if (info.tiktok && info.tiktok.length > 0) {
              $(".personal-bio-social-item:nth-child(4)").html(
                `<a href="${info.tiktok ?? 'https://tiktok.com'}" target="_blank">
                  <img src="/images/social/tiktok.png" alt="" />
                </a>`
              );
            // } else {
            //   $(".personal-bio-social-item:nth-child(4)").css(
            //     "display",
            //     "none"
            //   );
            // }
            $(".personal-bio-social-item:nth-child(5)").html(
              `<a href="${info?.website ?? 'https://google.com'}" target="_blank">
                <img src="/images/social/web.png" alt="" />
              </a>`
            );

            //tabs
            $(".tab").eq(1).removeClass("shimmerBG");
            $(".tab").eq(1).html(`
              Mã giảm giá
              <span class="notification">
                <i class="fas fa-piggy-bank"></i>
              </span>
            `);
            $(".tab").eq(0).removeClass("shimmerBG");
            $(".tab").eq(0).html(`
              Sản phẩm
              <span class="notification">
                <i class="fas fa-shopping-cart"></i>
              </span>
            `);
            $(".glider").css("background-color", "var(--ligh-orange-color)");

            $(".cdp").removeClass("shimmerBG");
            $(".cdp").css("background", "var(--ligh-orange-color)");
            $(".cdp").children().css("opacity", "1");
            $("html").fadeIn(500);
          });
        }, 1000);
      }
    });
}

function handleBioDesc() {
  $(".personal-bio-short-desc label").css("display", "inline-block");
  $(".personal-bio-short-desc label").html(
    $(".personal-bio-short-desc label").html() === "xem thêm"
      ? "ẩn bớt"
      : "xem thêm"
  );
}
