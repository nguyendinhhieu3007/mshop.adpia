@extends('layouts.app')
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('style')
    <style>
        .avatar-upload-file-button {
            background: blueviolet;
            padding: 10px;
            color: #ffffff;
            border-radius: 5px;
            width: fit-content;
            margin: auto;
            margin-top: 10px;
            transition: all .3s linear;
        }
        .avatar-upload-file-button:hover {
            background: darkorchid;
            cursor: pointer;
            -webkit-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.75);
            transform: translateY(-4px) scale(1.01);
        }
        .avatar-input-form-data {
            opacity: 0;
            /*height: 0;*/
        }
        .avatar-image-preview {
            height: 150px;
            width: 300px;
            margin: auto;
        }
        .avatar-image-preview img {
            height: 100%;
            width: 100%;
            /* object-fit: cover; */
        }
    </style>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Danh mục banner</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Danh mục banner</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="btn btn-app bg-success" style="float: right" id="btn_add" onclick="addBanner()"
                               title="Add" data-toggle="modal" data-target="#modal-add">
                                <i class="fas fa-plus"></i> Add
                            </a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover" style="width: 100%">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">STT</th>
                                    <th style="width: 15%;">Title</th>
                                    <th style="width: 25%;">Description</th>
                                    <th style="width: 35%;">Link</th>
                                    <th style="width: 10%;">Trạng thái</th>
                                    <th style="width: 10%;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($listBanner as $key => $value)
                                    <tr>
                                        <input type="hidden" id="image_{{ $value['id'] }}" value="{{ $value['url_image'] }}">
                                        <input type="hidden" id="url_link_{{ $value['id'] }}" value="{{ $value['link'] }}">
                                        <input type="hidden" id="banner_id_{{ $value['id'] }}" value="{{ $value['id'] }}">
                                        <input type="hidden" id="title_{{ $value['id'] }}" value="{{ $value['title'] }}">
                                        <input type="hidden" id="desc_{{ $value['id'] }}" value="{{ $value['description'] }}">
                                        <input type="hidden" id="edit_hide" value="">
                                        <td>{{ $key+1 }}</td>
                                        <td >{{ $value['title'] }}</td>
                                        <td >{{ $value['description'] }}</td>
                                        <td >{{ substr($value['link'],0,30) }}</td>
                                        <td >{{ $value['active'] == 1 ? 'Đang hoạt động' : 'Dừng hoạt động' }}</td>
                                        <td class="project-actions text-right">
                                            <button class="btn btn-info btn-sm" id="view"
                                                    onclick="viewBanner({{$value['id']}})" title="View"
                                                    data-toggle="modal" data-target="#modal-lg">
                                                <i class="fas fa-eye">
                                                </i>
                                            </button>
                                            <button class="btn btn-warning btn-sm" id="btn_edit"
                                                    onclick="editBanner({{$value['id']}})" title="Edit"
                                                    data-toggle="modal" data-target="#modal-edit">
                                                <i class="fas fa-pencil-alt">
                                                </i>
                                            </button>
                                            <button class="btn btn-danger btn-sm" id="btn_delete"
                                                    onclick="deleteBanner({{$value['id']}})" title="Delete"
                                            >
                                                <i class="fas fa-trash">
                                                </i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        {{--        modal view--}}
        <div class="modal fade" id="modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Xem banner</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card card-solid">
                                <div class="card-body">
                                    <div class="row">
                                        <hr>
                                        <div class="col-12 col-sm-6">
                                            <h3 class="d-inline-block d-sm-none"></h3>
                                            <div class="col-12">
                                                <img src="dist/img/prod-1.jpg" class="product-image" id="image" alt="Product Image">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="col-12" style="position: absolute; bottom: 0">
                                                <a href="https://shopee.vn" target="_blank" id="view_link"
                                                   class="btn btn-primary btn-lg btn-flat">
                                                    <i class="fas fa-cart-plus fa-lg mr-2"></i>
                                                    Tới liên kết
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--        modal add--}}
        <div class="modal fade" id="modal-add">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="addForm" enctype="multipart/form-data">
                        @csrf()
                        <div class="modal-header">
                            <h4 class="modal-title">Thêm banner</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card card-solid">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <label for="add_title">Title</label>
                                            <input type="text" class="form-control" id="add_title" name="add_title" required>
                                        </div>
                                        <div class="col-12">
                                            <label for="add_desc">Description</label>
                                            <input type="text" class="form-control" id="add_desc" name="add_desc" required>
                                        </div>
                                        <div class="col-12">
                                            <label for="add_link">Link</label>
                                            <input type="text" class="form-control" id="add_link" name="add_link" required>
                                        </div>
                                        <div class="col-12">
                                            <div class="p-top-10 avatar-upload-status">
                                                <span>Chọn file ảnh muốn tải lên</span>
                                            </div>
                                            <div class="avatar-image-preview">
                                                <img src="{{asset("/images/default_banner.png")}}" alt="">
                                                <input class="avatar-input-form-data" type="file" value="" class="form-control" name="url_image" id="add_image" required accept="image/*">
                                            </div>
                                            <div class="avatar-upload-file-button" id="add_title2">
                                                CHỌN FILE ẢNH
                                            </div>
                                        </div>

                                        <input type="hidden" id="add_image_hidden" value="">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary" id="btn_save_info">Lưu lại</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        {{--        modal edit--}}
        <div class="modal fade" id="modal-edit">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="editForm" enctype="multipart/form-data">
                        @csrf()
                        <div class="modal-header">
                            <h4 class="modal-title">Sửa Banner</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card card-solid">
                                <div class="card-body">
                                    <div class="row">
                                        <input type="text" id="id_hidden" value="" name="edit_banner_id" style="display: none">
                                        <div class="col-12">
                                            <label for="edit_title">Title</label>
                                            <input type="text" class="form-control" id="edit_title" name="edit_title" required>
                                        </div>
                                        <div class="col-12">
                                            <label for="edit_desc">Description</label>
                                            <input type="text" class="form-control" id="edit_desc" name="edit_desc" required>
                                        </div>
                                        <div class="col-12">
                                            <label for="edit_link">Link</label>
                                            <input type="text" class="form-control" id="edit_link" name="edit_link" required>
                                        </div>
                                        <div class="col-12">
                                            <div class="p-top-10 avatar-upload-status" id="edit_title1">
                                                <span>Chọn file ảnh muốn tải lên</span>
                                            </div>
                                            <div class="avatar-image-preview" id="edit_img">
                                                <img src="{{asset("/images/default_banner.png")}}" alt="">
                                                <input class="avatar-input-form-data" type="file" value="" class="form-control" name="url_image" id="edit_image"  accept="image/*">
                                            </div>
                                            <div class="avatar-upload-file-button" id="edit_title2">
                                                CHỌN FILE ẢNH
                                            </div>
                                        </div>

                                        <input type="hidden" id="edit_image_bk" name="edit_image_bk" value="">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary" id="btn_save_info">Lưu lại</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('script')
    <!-- DataTables  & Plugins -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="plugins/jszip/jszip.min.js"></script>
    <script src="plugins/pdfmake/pdfmake.min.js"></script>
    <script src="plugins/pdfmake/vfs_fonts.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
@endsection
@section('js')
    <script>
        $(function () {
            $("#example2").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                'aoColumnDefs': [
                    {
                        bSortable: false,
                        aTargets: [-1]
                    }
                ]
            }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
        });

        $("#add_title2").click(function(event) {
            $("#add_image").click();
        });
        $("#edit_title2").click(function(event) {
            $("#edit_image").click();
        });

        $(document).ready(function () {
            $('.product-image-thumb').on('click', function () {
                var $image_element = $(this).find('img')
                $('.product-image').prop('src', $image_element.attr('src'))
                $('.product-image-thumb.active').removeClass('active')
                $(this).addClass('active')
            })

            $('#product_name').dblclick(function () {
                $('#product_name').hide();
                $('#pro_show').show();
            })
        });

        function commonInfo() {
            if ($("#actionForm")[0].checkValidity()) {
                // var id_check = $('#id-edit').val();
                let product_name = $('#pro_show').val();
                let product_id = $('#id_hidden').val();
                console.log(product_name);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('product.edit') }}',
                    data: {product_name: product_name, product_id: product_id},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function (data) {
                        if (data[0] === 'error') {
                            alert(data[1]);
                        } else {
                            document.getElementById('actionForm').submit();
                        }
                    }
                })
            }
        }

        function deleteBanner(id) {
            if(confirm("Are you sure you want to delete this?")){
                $.ajax({
                    type: 'POST',
                    url: '{{ route('banner.delete') }}',
                    data: {id: id},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                })
                .done(function(res) {
                    if(res.status == 200) {
                        // clearInterval(timer);
                        $(".avatar-loading-text").html("Cập nhật thành công! Đang tiến hành load lại trang.");
                        window.location.reload();
                    } else {
                        toastr.error('Update thất bại, xin vui lòng thử lại sau');
                    }
                });
            }
            else{
                return false;
            }

        }

        function editBanner(id) {
            var image = $('#image_' + id).val();
            var url_link = $('#url_link_' + id).val();
            var banner_id = $('#banner_id_' + id).val();
            var title = $('#title_' + id).val();
            var desc = $('#desc_' + id).val();
            $('#id_hidden').val(banner_id);
            $('#edit_img img').attr('src', image);
            $('#edit_image_bk').val(image);
            $("#edit_title2").html("THAY ĐỔI FILE ẢNH");
            $('#edit_title').val(title);
            $('#edit_desc').val(desc);
            $('#edit_link').val(url_link);
        }

        function viewBanner(id) {
            var image = $('#image_' + id).val();
            var url_link = $('#url_link_' + id).val();
            // var banner_id = $('#banner_id_' + id).val();
            // var title = $('#title_' + id).val();
            // var desc = $('#desc_' + id).val();
            // var product_name = $('#product_name_' + id).text();
            // var merchant_id = $('#merchant_id_' + id).text();
            // var category_id = $('#category_id_' + id).text();
            // var price = $('#price_' + id).text();
            $('#image').attr('src', image);
            // $('#product_name').text(product_name);
            // $('#merchant_id').text(merchant_id);
            // $('#category_id').text(category_id);
            $('#view_link').attr('href',url_link);
            // $('#product_name').show();
            // $('#pro_show').hide();
        }

        function addBanner() {
            document.getElementById("addForm").reset();
            $(".avatar-upload-file-button").html("CHỌN FILE ẢNH");
            $(".avatar-upload-status").html("Chọn file ảnh muốn tải lên");
            $(".avatar-upload-status").css('color', '#000000');
            $(".avatar-image-preview img").attr("src", "");
        }

        function clearUpdateAvatarModal() {
            $(".avatar-image-preview").css({
                overflow: "hidden"
            });
            // $(".avatar-image-preview img").attr("src", "/images/avatar-uploading-icon.png");
            $(".avatar-upload-file-button").html("CHỌN FILE ẢNH");
            $(".avatar-upload-status").html("Chọn file ảnh muốn tải lên");
            $(".avatar-upload-status").css('color', '#000000');
            // $(".submit-update-avatar-button").attr("disabled", true);
        }

        $("#modal-add").on("change", "input[name=url_image]", function(event) {
            var file = this.files[0];
            if(typeof file == "undefined") {
                clearUpdateAvatarModal();
            } else {
                if(!['image/gif', 'image/jpeg', 'image/png'].includes(file["type"])) {
                    clearUpdateAvatarModal();
                    $(".avatar-upload-status").html("File bạn chọn không phải là file ảnh!");
                    $(".avatar-upload-status").css('color', '#ff0000');
                } else {
                    if(file["size"] > 2097152) {
                        clearUpdateAvatarModal();
                        $(".avatar-upload-status").html("File bạn chọn dung lượng vượt quá 2MB!");
                        $(".avatar-upload-status").css('color', '#ff0000');
                    } else {
                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function(e) {
                            $(".avatar-image-preview").css({
                                overflow: "hidden"
                            });

                            if (file["size"] > (1024 * 1024)) {
                                var fileSize = Math.floor(file["size"] / (1024 * 1024));
                                fileSize += "MB";
                            } else if(file["size"] > 1024) {
                                var fileSize = Math.floor(file["size"] / 1024);
                                fileSize += " KB";
                            }

                            var fileName = file["name"].length > 20 ? file["name"].substr(0, 20) + "..." : file["name"];
                            $(".avatar-image-preview img").attr("src", e.target.result);
                            $(".avatar-upload-file-button").html("THAY ĐỔI FILE ẢNH");
                            $(".avatar-upload-status").html("Tên : " + fileName + "<br>Dung lượng : " + fileSize);
                            $(".avatar-upload-status").css('color', '#000000');
                            $("#add_image_hidden").val(file["name"]);
                            // $(".submit-update-avatar-button").attr("disabled", false);
                        }
                    }
                }
            }
        });

        $("#modal-edit").on("change", "input[name=url_image]", function(event) {
            var file = this.files[0];
            console.log(file);
            if(typeof file == "undefined") {
                clearUpdateAvatarModal();
            } else {
                if(!['image/gif', 'image/jpeg', 'image/png'].includes(file["type"])) {
                    clearUpdateAvatarModal();
                    $(".avatar-upload-status").html("File bạn chọn không phải là file ảnh!");
                    $(".avatar-upload-status").css('color', '#ff0000');
                } else {
                    if(file["size"] > 2097152) {
                        clearUpdateAvatarModal();
                        $(".avatar-upload-status").html("File bạn chọn dung lượng vượt quá 2MB!");
                        $(".avatar-upload-status").css('color', '#ff0000');
                    } else {
                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function(e) {
                            $(".avatar-image-preview").css({
                                overflow: "hidden"
                            });

                            if (file["size"] > (1024 * 1024)) {
                                var fileSize = Math.floor(file["size"] / (1024 * 1024));
                                fileSize += "MB";
                            } else if(file["size"] > 1024) {
                                var fileSize = Math.floor(file["size"] / 1024);
                                fileSize += " KB";
                            }

                            var fileName = file["name"].length > 20 ? file["name"].substr(0, 20) + "..." : file["name"];
                            $(".avatar-image-preview img").attr("src", e.target.result);
                            $(".avatar-upload-file-button").html("THAY ĐỔI FILE ẢNH");
                            $(".avatar-upload-status").html("Tên : " + fileName + "<br>Dung lượng : " + fileSize);
                            $(".avatar-upload-status").css('color', '#000000');
                            $("#add_image_hidden").val(file["name"]);
                            // $(".submit-update-avatar-button").attr("disabled", false);
                        }
                    }
                }
            }
        });

        $('#addForm').submit(function(e) {
            e.preventDefault();
            let dots_num = 0;
            let dots_text = "";
            $("body").css({ pointerEvents: "none" });
            $("#modal_avatar .modal-body").animate({
                opacity: 0.1
            }, 500, function() {
                $(".avatar-loading-text").css("display", "block");
            });

            let timer = setInterval(function(){
                dots_text = "";
                for(var i = 0; i < dots_num; i++) {
                    dots_text += ".";
                }

                $(".avatar-loading-text").html("Đang cập nhật dữ liệu, xin vui lòng chờ" + dots_text);

                dots_num++;

                if(dots_num > 3) {
                    dots_num = 0;
                }
            }, 500);

            // var formElement = $("#addForm")[0];
            var formData = new FormData(this);
            $.ajax({
                url: '{{ route('banner.add') }}',
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            })
                .done(function(res) {
                    if(res.status == 200) {
                        clearInterval(timer);
                        $(".avatar-loading-text").html("Cập nhật thành công! Đang tiến hành load lại trang.");
                        window.location.reload();
                    } else {
                        toastr.error('Update thất bại, xin vui lòng thử lại sau');
                    }
                });
        });

        $('#editForm').submit(function(e) {
            e.preventDefault();
            let dots_num = 0;
            let dots_text = "";
            $("body").css({ pointerEvents: "none" });
            $("#modal_avatar .modal-body").animate({
                opacity: 0.1
            }, 500, function() {
                $(".avatar-loading-text").css("display", "block");
            });

            let timer = setInterval(function(){
                dots_text = "";
                for(var i = 0; i < dots_num; i++) {
                    dots_text += ".";
                }

                $(".avatar-loading-text").html("Đang cập nhật dữ liệu, xin vui lòng chờ" + dots_text);

                dots_num++;

                if(dots_num > 3) {
                    dots_num = 0;
                }
            }, 500);

            // var formElement = $("#addForm")[0];
            var formData = new FormData(this);
            $.ajax({
                url: '{{ route('banner.edit') }}',
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            })
                .done(function(res) {
                    if(res.status == 200) {
                        clearInterval(timer);
                        $(".avatar-loading-text").html("Cập nhật thành công! Đang tiến hành load lại trang.");
                        window.location.reload();
                    } else {
                        toastr.error('Update thất bại, xin vui lòng thử lại sau');
                    }
                });
        });
    </script>
@endsection
