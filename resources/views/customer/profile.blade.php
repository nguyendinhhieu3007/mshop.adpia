@extends('layouts.app')

@section('style')
    <style>
        .profile-user-img{
            height: 100px;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Profile</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User Profile</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <form id="update-image" method="post"  enctype="multipart/form-data">
                        @csrf()
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" id="add_image2"
                                     src="{{ $infoCustomer['avatar'] }}"
                                     alt="User profile picture">
                                <input type="file" value="" class="form-control" name="image" id="add_image" required accept="image/*" style="display: none">
                            </div>

                            <h3 class="profile-username text-center">{{ $infoCustomer['owner_name'] }}</h3>

                            <p class="text-muted text-center">{{ $infoCustomer['email'] }}</p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Shop name</b> <a class="float-right">{{ $infoCustomer['shop_name'] }}</a>
                                </li>
                                @if(!empty($infoCustomer['youtube']))
                                <li class="list-group-item">
                                    <b>Youtube</b> <a class="float-right">{{ $infoCustomer['youtube'] }}</a>
                                </li>
                                @endif
                                @if(!empty($infoCustomer['facebook']))
                                <li class="list-group-item">
                                    <b>Facebook</b> <a class="float-right">{{ $infoCustomer['facebook'] }}</a>
                                </li>
                                @endif
                                @if(!empty($infoCustomer['instagram']))
                                <li class="list-group-item">
                                    <b>Instagram</b> <a class="float-right">{{ $infoCustomer['instagram'] }}</a>
                                </li>
                                @endif
                                @if(!empty($infoCustomer['tiktok']))
                                <li class="list-group-item">
                                    <b>Tiktok</b> <a class="float-right">{{ $infoCustomer['tiktok'] }}</a>
                                </li>
                                @endif
                                @if(!empty($infoCustomer['website']))
                                <li class="list-group-item">
                                    <b>Website</b> <a class="float-right">{{ $infoCustomer['website'] }}</a>
                                </li>
                                @endif
                            </ul>
                            <button type="submit" id="button-update-image" class="btn btn-primary btn-block" style="display: none"><b>Cập nhật</b></button>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    </form>
                    <!-- /.card -->

                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">About Me</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <strong><i class="fas fa-book mr-1"></i> Description</strong>

                            <p class="text-muted">
                                {{ $infoCustomer['desc'] }}
                            </p>

                            <hr>

                            <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                            <p class="text-muted">{{ $infoCustomer['location'] }}</p>

                            <hr>

                            <strong><i class="fas fa-phone-alt mr-1"></i> Tel</strong>

                            <p class="text-muted">
                                {{ $infoCustomer['phone'] }}
                            </p>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <h3 class="nav nav-pills">Thông tin cá nhân</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="settings">
                                    <!-- Post -->
                                    <div class="post">

                                        <form class="form-horizontal" method="post" id="profile-form">
                                            @csrf()
                                            <div class="form-group row">
                                                <label for="shop_name" class="col-sm-2 col-form-label">Shop name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="shop_name" name="shop_name" required
                                                           value="{{ $infoCustomer['shop_name'] }}" placeholder="Shop name">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="owner_name" class="col-sm-2 col-form-label">Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="owner_name" name="owner_name" required
                                                           value="{{ $infoCustomer['owner_name'] }}" placeholder="Name">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input type="email" class="form-control" id="email" name="email" required
                                                           value="{{ $infoCustomer['email'] }}" placeholder="Email">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="phone" name="phone" required
                                                           value="{{ $infoCustomer['phone'] }}" placeholder="Phone">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="location" class="col-sm-2 col-form-label">Location</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="location" name="location"
                                                           value="{{ $infoCustomer['location'] }}" placeholder="Location">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="desc"
                                                       class="col-sm-2 col-form-label">Description</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" id="desc" name="desc" 
                                                              placeholder="Experience">{{ $infoCustomer['desc'] }}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="youtube" class="col-sm-2 col-form-label">Youtube</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="youtube" name="youtube"
                                                           value="{{ $infoCustomer['youtube'] }}" placeholder="Youtube">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="facebook" class="col-sm-2 col-form-label">Facebook</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="facebook" name="facebook"
                                                           value="{{ $infoCustomer['facebook'] }}" placeholder="Facebook">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="instagram" class="col-sm-2 col-form-label">Instagram</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="instagram" name="instagram"
                                                           value="{{ $infoCustomer['instagram'] }}" placeholder="Instagram">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tiktok" class="col-sm-2 col-form-label">Tiktok</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="tiktok" name="tiktok"
                                                           value="{{ $infoCustomer['tiktok'] }}" placeholder="Tiktok">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="website" class="col-sm-2 col-form-label">Website</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="website" name="website"
                                                           value="{{ $infoCustomer['website'] }}" placeholder="Website">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="offset-sm-2 col-sm-10">
                                                    <button type="submit" class="btn btn-danger">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.post -->
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('js')
    <script>
        $("#add_image2").click(function(event) {
            $("#add_image").click();
        });
        $(function () {
            $('#add_image').change(function () {
                $('#button-update-image').show();
            })
        });

        $("#add_image").on("change", function(event) {
            var file = this.files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e) {
                $("#add_image2").attr("src", e.target.result);
            }
        });

        $('#update-image').submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: '{{ route('customer.avatar') }}',
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                },
                error: function(jqXHR, textStatus, errorThrown) {
                }
            })
                .done(function(res) {
                    if(res.status == 200) {
                        window.location.reload();
                    } else {
                        alert('Thêm mới thất bại, xin vui lòng thử lại sau');
                    }
                });
        });
        $('#profile-form').submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url: '{{ route('customer.edit') }}',
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                },
                error: function(jqXHR, textStatus, errorThrown) {
                }
            })
                .done(function(res) {
                    if(res.status == 200) {
                        window.location.reload();
                    } else {
                        alert('Thêm mới thất bại, xin vui lòng thử lại sau');
                    }
                });
        });
    </script>
@endsection
