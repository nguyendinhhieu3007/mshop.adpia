<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{asset('/images/logo.png')}}" alt="Adpia Logo" class="brand-image elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Quản Lý Page</span>
    </a>
    @php
    $user = \Auth::user();
    @endphp
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{$user->avatar}}" class="img-circle elevation-2" alt="User Image" style="width: 35px;height: 35px">
            </div>
            <div class="info">
                <a href="{{route('profile')}}" class="d-block">Profile</a>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item menu-open">
                    <a href="{{ route('minishop',["slug" => $user->shop_name_slug]) }}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{$user->shop_name}}
                        </p>
                    </a>
                </li>

                <li class="nav-header">Products</li>
                <li class="nav-item">
                    <a href="{{ route('product.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Danh mục sản phẩm
                        </p>
                    </a>
                </li>

{{--                <li class="nav-header">Coupons</li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="{{ route('product.index') }}" class="nav-link">--}}
{{--                        <i class="nav-icon fas fa-tags"></i>--}}
{{--                        <p>--}}
{{--                            Danh mục mã giảm giá--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                </li>--}}

                <li class="nav-header">Banner</li>
                <li class="nav-item">
                    <a href="{{ route('banner.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Danh mục banner
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
