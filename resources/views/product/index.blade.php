@extends('layouts.app')
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('style')
    <style>
        .avatar-upload-file-button {
            background: blueviolet;
            padding: 10px;
            color: #ffffff;
            border-radius: 5px;
            width: fit-content;
            margin: auto;
            margin-top: 10px;
            transition: all .3s linear;
        }
        .avatar-upload-file-button:hover {
            background: darkorchid;
            cursor: pointer;
            -webkit-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.75);
            transform: translateY(-4px) scale(1.01);
        }
        .avatar-input-form-data {
            opacity: 0;
            /*height: 0;*/
        }
        .avatar-image-preview {
            height: 128px;
            width: 128px;
            margin: auto;
        }
        .avatar-image-preview img {
            height: 100%;
            width: 100%;
            object-fit: cover;
        }
    </style>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Danh mục sản phẩm</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Danh mục sản phẩm</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a class="btn btn-app bg-success" style="float: right" id="btn_add" onclick="addProduct()"
                                title="Add" data-toggle="modal" data-target="#modal-add">
                                <i class="fas fa-plus"></i> Add
                            </a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover" style="width: 100%">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 5%;">STT</th>
                                    <th class="text-center" style="width: 5%;">Merchant</th>
                                    <th class="text-center">Category</th>
                                    <th class="text-center" style="width: 50%;">Tên sản phẩm</th>
                                    <th class="text-center w-10">Giá</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($listProduct as $key => $value)
                                    <tr>
                                        <input type="hidden" id="image_{{ $value['id'] }}"
                                               value="{{ $value['image'] }}">
                                        <input type="hidden" id="url_link_{{ $value['id'] }}" value="{{ $value['url'] }}">
                                        <input type="hidden" id="product_id_{{ $value['id'] }}" value="{{ $value['id'] }}">
                                        <input type="hidden" id="category_id_{{ $value['id'] }}" value="{{ $value['category_id'] }}">
                                        <input type="hidden" id="merchant_id_{{ $value['id'] }}" value="{{ $value['merchant_id'] }}">
                                        <input type="hidden" id="price_{{ $value['id'] }}" value="{{ $value['price'] }}">
                                        <input type="hidden" id="url_tracking_{{ $value['id'] }}" value="{{ urldecode($value['url_tracking']) }}">
                                        <input type="hidden" id="edit_hide" value="">
                                        <td class="text-center">{{ $key+1 }}</td>
                                        <td id="" class="text-center">{{ $value['merchant_id'] }}</td>
                                        <td id="category_name_{{ $value['id'] }}">{{ $value['category_name'] }}</td>
                                        <td id="product_name_{{ $value['id'] }}">{{ $value['product_name'] }}</td>
                                        <td class="text-right" >{{ number_format($value['price'], 0, '', ',') }} đ</td>
                                        <td style="width: 140px;min-width:140px;box-sizing: border-box;" class="text-center">
                                            <button class="btn btn-info btn-sm" id="view"
                                                    onclick="viewProduct({{$value['id']}})" title="View"
                                                    data-toggle="modal" data-target="#modal-lg">
                                                <i class="fas fa-eye">
                                                </i>
                                            </button>
                                            <button class="btn btn-warning btn-sm" id="btn_edit"
                                                    onclick="editProduct({{$value['id']}})" title="Edit"
                                                    data-toggle="modal" data-target="#modal-edit">
                                                <i class="fas fa-pencil-alt">
                                                </i>
                                            </button>
                                            <button class="btn btn-danger btn-sm" id="btn_delete"
                                                             onclick="deleteProduct({{$value['id']}})" title="Delete"
                                                             >
                                                <i class="fas fa-trash">
                                                </i>
                                            </button>
{{--                                            <a class="btn btn-danger btn-sm" href="#" title="Delete">--}}
{{--                                                <i class="fas fa-trash">--}}
{{--                                                </i>--}}
{{--                                            </a>--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

{{--        modal view--}}
        <div class="modal fade" id="modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Xem sản phẩm</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card card-solid">
                                <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <h5 id="category_id"></h5>
                                            </div>
                                            <hr>
                                            <div class="col-12 col-sm-6">
                                                <h3 class="d-inline-block d-sm-none"></h3>
                                                <div class="col-12">
                                                    <img src="dist/img/prod-1.jpg" class="product-image" id="image" alt="Product Image">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <h3 class="my-3" id="product_name"></h3>
                                                <textarea name="product_name" id="pro_show" style="width: 100%;font-size: 25px;display: none" rows="3"></textarea>
                                                <p id="merchant_id"></p>
                                                <hr>
                                                <div class="py-2 px-3 mt-4">
                                                    <h2 class="mb-0" id="price"></h2>
                                                </div>
                                                <div class="mt-4" style="position: absolute; bottom: 0">
                                                    <a href="https://shopee.vn" target="_blank" id="url_link"
                                                       class="btn btn-primary btn-lg btn-flat">
                                                        <i class="fas fa-cart-plus fa-lg mr-2"></i>
                                                        Tới cửa hàng
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
{{--        modal add--}}
        <div class="modal fade" id="modal-add">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="addForm" enctype="multipart/form-data">
                        @csrf()
                        <div class="modal-header">
                            <h4 class="modal-title">Thêm sản phẩm</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card card-solid">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <label for="add_product_name">Tên sản phẩm</label>
                                            <input type="text" class="form-control" id="add_product_name" name="add_product_name" required>
                                        </div>
                                        <div class="col-12">
                                            <label for="add_category">Danh mục</label>
                                            <select name="add_category" id="add_category" class="form-control" required>
                                                <option value="">Chọn danh mục</option>
                                                @foreach($listCategory as $list => $item)
                                                    <option value="{{ $item['id'] }}">{{ $item['category_name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <label for="add_merchant">Nhà cung cấp</label>
                                            <select name="add_merchant" id="add_merchant" class="form-control" required>
                                                <option value="">Chọn nhà cung cấp</option>
                                                <option value="tiki">Tiki</option>
                                                <option value="shopee">Shopee</option>
                                                <option value="lazada">Lazada</option>
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <label for="add_price">Giá</label>
                                            <input type="text" class="form-control" id="add_price" name="add_price" required>
                                        </div>
                                        <div class="col-12">
                                            <label for="add_url">Link</label>
                                            <input type="text" class="form-control" id="add_url" name="add_url" required>
                                        </div>
                                        <div class="col-12">
                                            <div class="p-top-10 avatar-upload-status">
                                                <span>Chọn file ảnh muốn tải lên</span>
                                            </div>
                                            <div class="avatar-image-preview">
                                                <img alt="">
                                                <input class="avatar-input-form-data" type="file" value="" class="form-control" name="image" id="add_image" required accept="image/*">
                                            </div>
                                            <div class="avatar-upload-file-button" id="add_title2">
                                                CHỌN FILE ẢNH
                                            </div>
                                        </div>

                                        <input type="hidden" id="add_image_hidden" value="">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary" id="btn_save_info">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="display: none"></span>
                                Lưu lại</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
{{--        modal edit--}}
        <div class="modal fade" id="modal-edit">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="editForm" enctype="multipart/form-data">
                        @csrf()
                        <div class="modal-header">
                            <h4 class="modal-title">Sửa sản phẩm</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card card-solid">
                                <div class="card-body">
                                    <div class="row">
                                        <input type="text" id="id_hidden" value="" name="edit_product_id" style="display: none">
                                        <div class="col-12">
                                            <label for="edit_product_name">Tên sản phẩm</label>
                                            <input type="text" class="form-control" id="edit_product_name" name="edit_product_name" required>
                                        </div>
                                        <div class="col-12">
                                            <label for="edit_category">Danh mục</label>
                                            <select name="edit_category" id="edit_category" class="form-control" required>
                                                <option value="">Chọn danh mục</option>
                                                @foreach($listCategory as $list => $item)
                                                    <option value="{{ $item['id'] }}">{{ $item['category_name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <label for="edit_merchant">Nhà cung cấp</label>
                                            <select name="edit_merchant" id="edit_merchant" class="form-control" required>
                                                <option value="">Chọn nhà cung cấp</option>
                                                <option value="tiki">Tiki</option>
                                                <option value="shopee">Shopee</option>
                                                <option value="lazada">Lazada</option>
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <label for="edit_price">Giá</label>
                                            <input type="text" class="form-control" id="edit_price" name="edit_price" required>
                                        </div>
                                        <div class="col-12">
                                            <label for="edit_url">Link</label>
                                            <input type="text" class="form-control" id="edit_url" name="edit_url" required>
                                        </div>
                                        <div class="col-12">
                                            <div class="p-top-10 avatar-upload-status" id="edit_title1">
                                                <span>Chọn file ảnh muốn tải lên</span>
                                            </div>
                                            <div class="avatar-image-preview" id="edit_img">
                                                <img alt="">
                                            </div>
                                            <div class="avatar-upload-file-button" id="edit_title2">
                                                CHỌN FILE ẢNH
                                            </div>
                                        </div>
                                        <input class="avatar-input-form-data" type="file" value="" class="form-control" name="image" id="edit_image"  accept="image/*">
                                        <input type="hidden" id="edit_image_bk" name="edit_image_bk" value="">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary" id="btn_save_info">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="display: none"></span>
                                Lưu lại</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('script')
    <!-- DataTables  & Plugins -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="plugins/jszip/jszip.min.js"></script>
    <script src="plugins/pdfmake/pdfmake.min.js"></script>
    <script src="plugins/pdfmake/vfs_fonts.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
@endsection
@section('js')
    <script>
        $(function () {
            $("#example2").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                'aoColumnDefs': [
                    {
                        bSortable: false,
                        aTargets: [-1]
                    }
                ]
            }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
        });

        $("#add_title2").click(function(event) {
            $("#add_image").click();
        });
        $("#edit_title2").click(function(event) {
            $("#edit_image").click();
        });

        $(document).ready(function () {
            $('.product-image-thumb').on('click', function () {
                var $image_element = $(this).find('img')
                $('.product-image').prop('src', $image_element.attr('src'))
                $('.product-image-thumb.active').removeClass('active')
                $(this).addClass('active')
            })

            // $('#product_name').dblclick(function () {
            //     $('#product_name').hide();
            //     $('#pro_show').show();
            // })
        });

        function commonInfo() {
            if ($("#actionForm")[0].checkValidity()) {
                // var id_check = $('#id-edit').val();
                let product_name = $('#pro_show').val();
                let product_id = $('#id_hidden').val();
                console.log(product_name);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('product.edit') }}',
                    data: {product_name: product_name, product_id: product_id},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function (data) {
                        if (data[0] === 'error') {
                            alert(data[1]);
                        } else {
                            document.getElementById('actionForm').submit();
                        }
                    }
                })
            }
        }

        function deleteProduct(id) {
            if(confirm("Are you sure you want to delete this?")){
                $.ajax({
                    type: 'POST',
                    url: '{{ route('product.delete') }}',
                    data: {id: id},
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function (data) {
                        // if (data[0] === 'error') {
                        //     alert(data[1]);
                        // } else {
                        //     document.getElementById('addForm').submit();
                        // }
                    }
                })
                    .done(function(res) {
                        if(res.status == 200) {
                            // clearInterval(timer);
                            $(".avatar-loading-text").html("Cập nhật thành công! Đang tiến hành load lại trang.");
                            window.location.reload();
                        } else {
                            toastr.error('Update thất bại, xin vui lòng thử lại sau');
                        }
                    });
            }
            else{
                return false;
            }

        }

        function editProduct(id) {
            var image = $('#image_' + id).val();
            var product_name = $('#product_name_' + id).text();
            var merchant_id = $('#merchant_id_' + id).val();
            var category_id = $('#category_id_' + id).val();
            var price = $('#price_' + id).val();
            var url = $('#url_link_' + id).val();
            var product_id = $('#product_id_' + id).val();
            $('#id_hidden').val(product_id);
            $('#edit_img img').attr('src', image);
            $('#edit_image_bk').val(image);
            $("#edit_title2").html("THAY ĐỔI FILE ẢNH");
            $('#edit_product_name').val(product_name);
            $('#edit_merchant').val(merchant_id);
            $('#edit_category').val(category_id);
            $('#edit_price').val(price);
            $('#edit_url').val(url);

            console.log(product_id);
        }

        function viewProduct(id) {
            var image = $('#image_' + id).val();
            var product_name = $('#product_name_' + id).text();
            var merchant_id = $('#merchant_id_' + id).val();
            var category_id = $('#category_name_' + id).text();
            var price = $('#price_' + id).val();
            var url = $('#url_tracking_' + id).val();
            $('#image').attr('src', image);
            $('#product_name').text(product_name);
            $('#merchant_id').text(merchant_id);
            $('#category_id').text(category_id);
            $('#price').text(price);
            $('#url_link').attr('href',url);
            // $('#product_name').show();s
            // $('#pro_show').hide();
        }

        function addProduct() {
            document.getElementById("addForm").reset();
            $(".avatar-upload-file-button").html("CHỌN FILE ẢNH");
            $(".avatar-upload-status").html("Chọn file ảnh muốn tải lên");
            $(".avatar-upload-status").css('color', '#000000');
            $(".avatar-image-preview img").attr("src", "");
        }

        function clearUpdateAvatarModal() {
            $(".avatar-image-preview").css({
                overflow: "hidden"
            });
            // $(".avatar-image-preview img").attr("src", "/images/avatar-uploading-icon.png");
            $(".avatar-upload-file-button").html("CHỌN FILE ẢNH");
            $(".avatar-upload-status").html("Chọn file ảnh muốn tải lên");
            $(".avatar-upload-status").css('color', '#000000');
            // $(".submit-update-avatar-button").attr("disabled", true);
        }

        $("#modal-add").on("change", "input[name=image]", function(event) {
            var file = this.files[0];
            if(typeof file == "undefined") {
                clearUpdateAvatarModal();
            } else {
                if(!['image/gif', 'image/jpeg', 'image/png'].includes(file["type"])) {
                    clearUpdateAvatarModal();
                    $(".avatar-upload-status").html("File bạn chọn không phải là file ảnh!");
                    $(".avatar-upload-status").css('color', '#ff0000');
                } else {
                    if(file["size"] > 2097152) {
                        clearUpdateAvatarModal();
                        $(".avatar-upload-status").html("File bạn chọn dung lượng vượt quá 2MB!");
                        $(".avatar-upload-status").css('color', '#ff0000');
                    } else {
                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function(e) {
                            $(".avatar-image-preview").css({
                                overflow: "hidden"
                            });

                            if (file["size"] > (1024 * 1024)) {
                                var fileSize = Math.floor(file["size"] / (1024 * 1024));
                                fileSize += "MB";
                            } else if(file["size"] > 1024) {
                                var fileSize = Math.floor(file["size"] / 1024);
                                fileSize += " KB";
                            }

                            var fileName = file["name"].length > 20 ? file["name"].substr(0, 20) + "..." : file["name"];
                            $(".avatar-image-preview img").attr("src", e.target.result);
                            $(".avatar-upload-file-button").html("THAY ĐỔI FILE ẢNH");
                            $(".avatar-upload-status").html("Tên : " + fileName + "<br>Dung lượng : " + fileSize);
                            $(".avatar-upload-status").css('color', '#000000');
                            $("#add_image_hidden").val(file["name"]);
                            // $(".submit-update-avatar-button").attr("disabled", false);
                        }
                    }
                }
            }
        });

        $("#modal-edit").on("change", "input[name=image]", function(event) {
            var file = this.files[0];
            console.log(file);
            if(typeof file == "undefined") {
                clearUpdateAvatarModal();
            } else {
                if(!['image/gif', 'image/jpeg', 'image/png'].includes(file["type"])) {
                    clearUpdateAvatarModal();
                    $(".avatar-upload-status").html("File bạn chọn không phải là file ảnh!");
                    $(".avatar-upload-status").css('color', '#ff0000');
                } else {
                    if(file["size"] > 2097152) {
                        clearUpdateAvatarModal();
                        $(".avatar-upload-status").html("File bạn chọn dung lượng vượt quá 2MB!");
                        $(".avatar-upload-status").css('color', '#ff0000');
                    } else {
                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function(e) {
                            $(".avatar-image-preview").css({
                                overflow: "hidden"
                            });

                            if (file["size"] > (1024 * 1024)) {
                                var fileSize = Math.floor(file["size"] / (1024 * 1024));
                                fileSize += "MB";
                            } else if(file["size"] > 1024) {
                                var fileSize = Math.floor(file["size"] / 1024);
                                fileSize += " KB";
                            }

                            var fileName = file["name"].length > 20 ? file["name"].substr(0, 20) + "..." : file["name"];
                            $(".avatar-image-preview img").attr("src", e.target.result);
                            $(".avatar-upload-file-button").html("THAY ĐỔI FILE ẢNH");
                            $(".avatar-upload-status").html("Tên : " + fileName + "<br>Dung lượng : " + fileSize);
                            $(".avatar-upload-status").css('color', '#000000');
                            $("#add_image_hidden").val(file["name"]);
                            // $(".submit-update-avatar-button").attr("disabled", false);
                        }
                    }
                }
            }
        });

        $('#addForm').submit(function(e) {
            e.preventDefault();
            $('.spinner-border').css('display','');
            $('#btn_save_info').attr('disabled', true);

            // var formElement = $("#addForm")[0];
            var formData = new FormData(this);
            $.ajax({
                url: '{{ route('product.add') }}',
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#add_url').css('border-color', '');
                    $('#add_merchant').css('border-color', '');
                    $('.spinner-border').css('display','none');
                    $('#btn_save_info').attr('disabled', false);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#add_url').css('border-color', 'red');
                    $('#add_merchant').css('border-color', 'red');
                    alert('Đường dẫn sản phẩm phải thuộc nhà cung cấp! Vui lòng kiểm tra lại');
                    $('.spinner-border').css('display','none');
                    $('#btn_save_info').attr('disabled', false);
                }
            })
            .done(function(res) {
                if(res.status == 200) {
                    // clearInterval(timer);
                    // $(".avatar-loading-text").html("Cập nhật thành công! Đang tiến hành load lại trang.");
                    window.location.reload();
                } else {
                    alert('Thêm mới thất bại, xin vui lòng thử lại sau');
                }
            });
        });

        $('#editForm').submit(function(e) {
            e.preventDefault();
            $('.spinner-border').css('display','');
            $('#btn_save_info').attr('disabled', true);

            // var formElement = $("#addForm")[0];
            var formData = new FormData(this);
            $.ajax({
                url: '{{ route('product.edit') }}',
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#edit_url').css('border-color', '');
                    $('#edit_merchant').css('border-color', '');
                    $('.spinner-border').css('display','none');
                    $('#btn_save_info').attr('disabled', false);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#edit_url').css('border-color', 'red');
                    $('#edit_merchant').css('border-color', 'red');
                    alert('Đường dẫn sản phẩm phải thuộc nhà cung cấp! Vui lòng kiểm tra lại');
                    $('.spinner-border').css('display','none');
                    $('#btn_save_info').attr('disabled', false);
                }
            })
            .done(function(res) {
                if(res.status == 200) {
                    // clearInterval(timer);
                    // $(".avatar-loading-text").html("Cập nhật thành công! Đang tiến hành load lại trang.");
                    window.location.reload();
                } else {
                    alert('Cập nhật thất bại, xin vui lòng thử lại sau');
                }
            });
        });

    </script>
@endsection
