<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/customer', function () {
    return view('customer.index');
});
//Login
Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'index'])->name('login');
Route::post('/login',  [App\Http\Controllers\Auth\LoginController::class, 'login']);

//Logout
//Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
//Customer
    Route::get('/profile',  'Customer\CustomerController@customerProfile')->name('profile');
    Route::post('customer/edit',  'Customer\CustomerController@editCustomer')->name('customer.edit');
    Route::post('customer/avatar',  'Customer\CustomerController@changeAvatar')->name('customer.avatar');

//Product
    Route::get('/product',  'Product\ProductController@index')->name('product.index');
    Route::post('/edit',  'Product\ProductController@editProduct')->name('product.edit');
    Route::post('/add',  'Product\ProductController@addProduct')->name('product.add');
    Route::post('/delete',  'Product\ProductController@deleteProduct')->name('product.delete');
    Route::post('/util/do_upload', 'Product\ProductController@util_do_upload')->name('util.do_upload');

//Coupon
    Route::get('/coupon',  'Customer\CustomerController@customerProfile')->name('coupon');

//    Banner
    Route::get('/banner',  'Banner\BannerController@index')->name('banner.index');
    Route::post('/banner/add',  'Banner\BannerController@addBanner')->name('banner.add');
    Route::post('banner/edit',  'Banner\BannerController@editBanner')->name('banner.edit');
    Route::post('banner/delete',  'Banner\BannerController@deleteBanner')->name('banner.delete');
});

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');

Route::get('/minishop/{slug}', [App\Http\Controllers\MiniShopController::class, 'minishop'])->name('minishop');
